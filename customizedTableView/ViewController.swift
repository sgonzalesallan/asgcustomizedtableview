//
//  ViewController.swift
//  customizedTableView
//
//  Created by Allan Gonzales on 3/27/15.
//  Copyright (c) 2015 Allan Gonzales. All rights reserved.
//

import UIKit

class ViewController : UITableViewController, UITableViewDataSource, UITableViewDelegate {

    let business : [String] = ["Business", "Twitter", "Email"] as [String]
    let businessDetail = ["IOS","@notagainallan","s.gonzalesallan@gmail.com"]
    let appFeedBack : [String] = ["Take our survey", "info@critics.com"] as [String]
    let titleHeader : [String] = ["My Business", "Account", "Email Alerts", "App Feedback"] as [String]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return business.count
        } else if section == 3 {
            return appFeedBack.count
        } else {
            return 1
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return titleHeader.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : UITableViewCell?
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("CellWithTextField", forIndexPath: indexPath)
                as? UITableViewCell
            var titleLabel = cell?.viewWithTag(10) as UILabel
            var textField = cell?.viewWithTag(11) as UITextField
            titleLabel.text = business[indexPath.row]
            textField.text = businessDetail[indexPath.row]
            
        } else if indexPath.section == 1 {
            cell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath)
                as? UITableViewCell
            cell?.textLabel?.text = "Upgrade Accout"
        } else if indexPath.section == 2 {
            cell = tableView.dequeueReusableCellWithIdentifier("CellWithSwitch", forIndexPath: indexPath)
                as? UITableViewCell
        } else if indexPath.section == 3 {
            cell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath)
                as? UITableViewCell
            cell?.textLabel?.text = appFeedBack[indexPath.row]
        }
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return titleHeader[section]
    }


}

